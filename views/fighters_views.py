from flask import Blueprint, render_template
from fighters_schema_model import *


fighters_app = Blueprint('fighters_app', __name__)


@fighters_app.route('/', endpoint='fighters')
def bill_of_products_page():
    response = render_template(
        'bill_of_products_page.html',
        all_fighters=names,
        number_of_fighters=len(names)
    )
    return response


@fighters_app.route('/<int:fighter_id>/', endpoint='fighter')
def product_page(fighter_id):
    fighter_script = Fighter(fighter_id)
    response = render_template(
        'product_page.html',
        product_id=fighter_id,
        fighter_script=fighter_script.fighter_parameters(),
        fighter_name=fighter_script.name,
    )
    return response



