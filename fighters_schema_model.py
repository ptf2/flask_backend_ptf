from postgres_editor import *
from json import JSONEncoder


class Fighter:
    def __init__(self, fighter_number):
        self.fighter_number = fighter_number
        self.id = ids[self.fighter_number]
        self.name = names[self.fighter_number]
        self.last_name = last_names[self.fighter_number]
        self.age = ages[self.fighter_number]
        self.height = float(heights[self.fighter_number])
        self.weight = float(weights[self.fighter_number])

    def fighter_parameters(self):
        json_enc = JSONEncoder()
        return json_enc.encode({"id": self.id,
                                "name": self.name,
                                "last_name": self.last_name,
                                "age": self.age,
                                "height": self.height,
                                "weight": self.weight})


try:
    db_editor = DatabaseEditor(database="ptf_db",
                               user='postgres',
                               password='123',
                               host="127.0.0.1",
                               port='5432')

    ids = db_editor.read_chosen_column(table_name='fighters_schema', column_name='id')
    names = db_editor.read_chosen_column(table_name='fighters_schema', column_name='name')
    last_names = db_editor.read_chosen_column(table_name='fighters_schema', column_name='lastName')
    ages = db_editor.read_chosen_column(table_name='fighters_schema', column_name='age')
    heights = db_editor.read_chosen_column(table_name='fighters_schema', column_name='height')
    weights = db_editor.read_chosen_column(table_name='fighters_schema', column_name='weight')

except Exception as e:
    logging.error(e)

else:
    db_editor.close_connection()
