from flask import Flask, render_template
from views.fighters_views import fighters_app


app = Flask(__name__)
app.register_blueprint(fighters_app, url_prefix='/fighters/')


@app.route('/')
def index_page():
    response = render_template(
        'index.html',
    )
    return response


class UncaughtExceptions(Exception):
    pass


if __name__ == '__main__':
    app.run('localhost', 8080, debug=True)
