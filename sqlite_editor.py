# -*- coding: utf-8 -*-
import sqlite3
import os
import logging

# logging.basicConfig(format=u'%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s] %(message)s',
#                     level=logging.DEBUG, filename=u'mylog.log')
# logging.basicConfig(format='%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s',
#                     level=logging.DEBUG)
logging.basicConfig(format='%(filename)s[LINE:%(lineno)d]# %(levelname)-8s %(message)s',
                    level=logging.DEBUG)


class DatabaseEditor:
    def __init__(self, db_directory):
        self.db_directory = db_directory
        self.table_name = None
        self.table_columns = ()
        self.table_data = None
        self.connection = None
        self.cursor = None
        self.open_connection()

    def open_connection(self):
        try:
            self.connection = sqlite3.connect(self.db_directory)
            self.cursor = self.connection.cursor()
        except TypeError as e:
            logging.error(e)
            logging.info('''to connect to the existing sqlite database or create new one:
            please enter a full name of the .db file
            for example: db_editor = DatabaseEditor("new_db.db")''')
            quit()
        except sqlite3.Error as sql_error:
            logging.error(f'failed to connect to sqlite database: {sql_error}')
        else:
            logging.info(f'connected to {self.db_directory}')

    def create_table(self, table_name='new_table', table_columns=("id", "user TEXT")):
        try:
            if table_columns[0] != 'id':
                raise ValueError('first column can only be named as "id"')

        except ValueError as e:
            logging.error(e)

        except sqlite3.Error as sql_error:
            logging.error(f'failed to create sqlite table "{table_name}": {sql_error}')

        else:
            self.table_columns = table_columns
            self.table_name = table_name
            query = 'CREATE TABLE IF NOT EXISTS ' + self.table_name + str(self.table_columns)
            self.cursor.execute(query)
            self.connection.commit()

    def insert_data(self, table_name, values=()):
        about = f'''inserted data must be a tuple
                    take a look at the table '{table_name}' and its columns in the list of \
                    all tables in '{self.db_directory}' database:'''
        try:
            if type(values[0]) is not int:
                raise ValueError(f'id-column can be an integer type only')

            # if self.read_chosen_column(table_name, "id"):
            if not values[0] in self.read_chosen_column(table_name, "id"):
                self.table_name = table_name
                self.cursor.execute(f'INSERT INTO {self.table_name} VALUES {str(values)}')
                self.connection.commit()
            else:
                raise ValueError(f'record with id={values[0]} already exists in {table_name} table')

        except ValueError as e:
            logging.error(e)

        except (TypeError, sqlite3.OperationalError) as e:
            logging.error(e)
            logging.info(about)
            self.watch_all_tables()

        except sqlite3.Error as sql_error:
            logging.error(f"failed to insert data to sqlite table: {sql_error}")

    def update_data(self, table='table_name', column='column_name', new_record='data', id_number=1):
        try:
            self.table_name = table

            if column == 'id' and type(new_record) is not int:
                raise ValueError('id-column can be an integer type only')

        except ValueError as e:
            logging.error(e)

        except sqlite3.Error as sql_error:
            logging.error(f"failed to update data to sqlite table: {sql_error}")

        else:
            if new_record != str(new_record):
                sql = f"UPDATE {table} SET '{column}' = {new_record} WHERE id = {id_number}"

            else:
                sql = f"UPDATE {table} SET '{column}' = '{new_record}' WHERE id = {id_number}"

            self.cursor.execute(sql)
            self.connection.commit()

    def read_table(self, table_name):
        self.table_name = table_name
        self.cursor.execute(f'SELECT * FROM {self.table_name}')
        self.table_data = self.cursor.fetchall()
        return self.table_data

    def read_chosen_column(self, table_name, column_name=''):
        try:
            self.table_name = table_name
            self.cursor.execute(f"SELECT {column_name} FROM {self.table_name}")
        except sqlite3.Error as sql_error:
            logging.error(f"failed to read column from sqlite table: {sql_error}")
        else:
            data_list_of_tuples = self.cursor.fetchall()

            data_list = []
            for i in range(len(data_list_of_tuples)):
                data_list.append(data_list_of_tuples[i][0])
            return data_list

    def watch_all_tables(self):
        os.system('sqlite3 ' + self.db_directory + ' .schema')

    def del_table(self, table_name):
        self.table_name = table_name
        self.cursor.execute(f'DROP TABLE {self.table_name}')
        self.connection.commit()

    def close_connection(self):
        self.cursor.close()
        self.connection.close()
        logging.info("sqlite connection is closed")

    def del_record_from_table(self, table, record_id):
        query = f"DELETE FROM {table} WHERE id = {record_id}"
        self.cursor.execute(query)
        self.connection.commit()

    #
    # def join_tables_for_fk(self):
    #     pass


if __name__ == '__main__':

    fields = ("id", "rating int", "name varchar(30)", "name_rus varchar(30)", "nickname varchar(30)",
                  "lastName varchar(30)", "lastName_rus varchar(30)", "age int", "record varchar(30)", "height numeric",
                  "weight numeric", "Базовый_стиль varchar(50)", "Родной_город varchar(50)", "Зал varchar(50)",
                  "Дебют DATE", "Победы_приемами int", "Победы_решениями int", "Финиши_в_первом_раунде int",
                  "Победы_нокаутами int", "Победная_серия int", "Размах_рук numeric", "Размах_ног numeric",
                  "Точность_ударов varchar(50)", "Нанесение_акц_ударов int", "Выброшенные_акц_удары int",
                  "Статистика_в_борьбе varchar(50)", "Тейкдаунов_выполнено int", "Тейкдауны_попыток int",
                  "Наносит_акц_ударов_за_мин numeric", "Пропускает_акц_ударов_за_мин numeric",
                  "Тейкдаунов_за_15_мин numeric", "Попыток_сабмишина_за_15_мин numeric",
                  "Защита_от_акц_ударов varchar(50)", "Защита_от_тейкдаунов varchar(50)", "Нокдаунов_за_бой_средн numeric",
                  "Среднее_время_боя TIME", "Акц_удары_в_стойке varchar(50)", "Акц_удары_в_клинче varchar(50)",
                  "Акц_удары_в_партере varchar(50)", "Акц_удары_цель_голова varchar(50)",
                  "Акц_удары_цель_Корпус varchar(50)", "Акц_удары_цель_Ноги varchar(50)", "Победы_КО_ТКО varchar(50)",
                  "Победы_решением varchar(50)", "Победы_сабмишен varchar(50))")

    db_editor = DatabaseEditor("fighters_base.db")
    db_editor.create_table('fighters', fields)
    logging.info(db_editor.watch_all_tables())
    # db_editor.cursor.execute("COPY fighters_schema FROM 'Baza_dannykh_po_boytsam_UFC.csv' WITH (FORMAT csv);")
    # db_editor.connection.commit()
    # db_editor.insert_data('fighters_schema', (1, 'Ryan', 'Benoit', 29, 165.1, 56.68))
    # db_editor.insert_data('fighters_schema', (2, 'Jarred', 'Brooks', 25, 160.01, 56.68))
    # db_editor.insert_data('fighters_schema', (3, 'Elias', 'Garsia', 26, 166.37, 56.68))
    # logging.info(db_editor.read_table('fighters_schema'))
    db_editor.close_connection()
