# -*- coding: utf-8 -*-
import psycopg2
import logging


logging.basicConfig(format='%(filename)s[LINE:%(lineno)d]# %(levelname)-8s %(message)s',
                    level=logging.DEBUG)


class DatabaseEditor:
    def __init__(self, *, database, user, password, host, port):
        self.database = database
        self.user = user
        self.password = password
        self.host = host
        self.port = port
        self.table_name = None
        self.table_columns = ()
        self.table_data = None
        self.connection = None
        self.cursor = None
        self.open_connection()

    def open_connection(self):
        try:
            self.connection = psycopg2.connect(
                database=self.database,
                user=self.user,
                password=self.password,
                host=self.host,
                port=self.port
            )
            self.cursor = self.connection.cursor()
        except psycopg2.Error as e:
            logging.error(e)
            quit()
        else:
            logging.info(f'connected to {self.database} database')

    def create_database(self, base_name):

        self.connection.autocommit = True
        self.cursor.execute(f'CREATE DATABASE {base_name}')
        logging.info(f"Database {base_name} created successfully")
        self.connection.commit()

    def create_table(self, table_name='new_table', table_columns=("id", "user TEXT")):
        # try:
        #     if table_columns[0] != 'id int':
        #         raise ValueError('first column can only be named as "id int"')
        #
        # except ValueError as e:
        #     logging.error(e)
        #
        # except psycopg2.Error as sql_error:
        #     logging.error(f'failed to create table "{table_name}": {sql_error}')
        #
        # else:
        self.table_columns = table_columns
        self.table_name = table_name
        query = f'CREATE TABLE IF NOT EXISTS {self.table_name} {self.table_columns}'
        self.cursor.execute(query)
        self.connection.commit()
        logging.info(f"Table {table_name} created successfully")

    def insert_data(self, table_name, values=()):
        about = f'''inserted data must be a tuple
                    take a look at the table '{table_name}' and its columns in the list of \
                    all tables in '{self.database}' database:'''
        try:
            if type(values[0]) is not int:
                raise ValueError(f'id-column can be an integer type only')

            if not values[0] in self.read_chosen_column(table_name, "id"):
                self.table_name = table_name
                self.cursor.execute(f'INSERT INTO {self.table_name} VALUES {str(values)}')
                self.connection.commit()
            else:
                raise ValueError(f'record with id={values[0]} already exists in {table_name} table')

        except ValueError as e:
            logging.error(e)

        except (TypeError, psycopg2.Error) as e:
            logging.error(f"failed to insert data in table: {sql_error}")
            logging.error(e)
            logging.info(about)
            # self.watch_all_tables()

    def import_data_from_csv_file(self, table, csv_file_path, delimiter=';', encoding='UTF8'):
        self.cursor.execute(
            f"COPY {table} FROM '{csv_file_path}' DELIMITER '{delimiter}' CSV HEADER ENCODING '{encoding}';")
        self.connection.commit()

    def update_data(self, table='table_name', column='column_name', new_record='data', id_number=1):
        try:
            self.table_name = table

            if column == 'id' and type(new_record) is not int:
                raise ValueError('id-column can be an integer type only')

        except ValueError as e:
            logging.error(e)

        except psycopg2.Error as sql_error:
            logging.error(f"failed to update data in table: {sql_error}")

        else:
            if new_record != str(new_record):
                sql = f"UPDATE {table} SET '{column}' = {new_record} WHERE id = {id_number}"

            else:
                sql = f"UPDATE {table} SET '{column}' = '{new_record}' WHERE id = {id_number}"

            self.cursor.execute(sql)
            self.connection.commit()

    def read_table(self, table_name):
        self.table_name = table_name
        self.cursor.execute(f'SELECT * FROM {self.table_name}')
        self.table_data = self.cursor.fetchall()
        return self.table_data

    def read_chosen_column(self, table_name, column_name=''):
        try:
            self.table_name = table_name
            self.cursor.execute(f"SELECT {column_name} FROM {self.table_name}")
        except psycopg2.Error as sql_error:
            logging.error(f"failed to read column from table: {sql_error}")
        else:
            data_list_of_tuples = self.cursor.fetchall()

            data_list = []
            for i in range(len(data_list_of_tuples)):
                data_list.append(data_list_of_tuples[i][0])
            return data_list

    # def watch_all_tables(self):
    #     system('sqlite3 ' + self.db_directory + ' .schema')
    #     cursor.execute(

    def del_record_from_table(self, table, record_id):
        query = f"DELETE FROM {table} WHERE id = {record_id}"
        self.cursor.execute(query)
        self.connection.commit()

    def del_table(self, table_name):
        self.table_name = table_name
        self.cursor.execute(f'DROP TABLE {self.table_name} ')
        self.connection.commit()

    def close_connection(self):
        self.cursor.close()
        self.connection.close()
        logging.info("Connection is closed")


if __name__ == '__main__':
    fields = "(id int, rating int, name varchar(30), name_rus varchar(30), nickname varchar(30), lastName varchar(30), \
lastName_rus varchar(30), age int, record varchar(30), height numeric, weight numeric, Базовый_стиль varchar(50), \
Родной_город varchar(50), Зал varchar(50), Дебют DATE, Победы_приемами int, Победы_решениями int, \
Финиши_в_первом_раунде int, Победы_нокаутами int, Победная_серия int, Размах_рук numeric, Размах_ног numeric, \
Точность_ударов varchar(50), Нанесение_акц_ударов int, Выброшенные_акц_удары int, Статистика_в_борьбе varchar(50), \
Тейкдаунов_выполнено int, Тейкдауны_попыток int, Наносит_акц_ударов_за_мин numeric, \
Пропускает_акц_ударов_за_мин numeric, Тейкдаунов_за_15_мин numeric, Попыток_сабмишина_за_15_мин numeric, \
Защита_от_акц_ударов varchar(50), Защита_от_тейкдаунов varchar(50), Нокдаунов_за_бой_средн numeric, \
Среднее_время_боя TIME, Акц_удары_в_стойке varchar(50), Акц_удары_в_клинче varchar(50), \
Акц_удары_в_партере varchar(50), Акц_удары_цель_голова varchar(50), Акц_удары_цель_Корпус varchar(50), \
Акц_удары_цель_Ноги varchar(50), Победы_КО_ТКО varchar(50), Победы_решением varchar(50), Победы_сабмишен varchar(50))"

    db_editor = DatabaseEditor(database="ptf_db",
                               user='postgres',
                               password='123',
                               host="127.0.0.1",
                               port='5432')


    # db_editor.import_data_from_csv_file(
    #     table='fighters_schema',
    #     csv_file_path='/home/anton/PycharmProjects/ptf_backend/app/UFC_FIGHTERS_DB.csv'
    # )

    ids = db_editor.read_chosen_column('fighters_schema', 'id')
    names = db_editor.read_chosen_column('fighters_schema', 'name')
    last_names = db_editor.read_chosen_column('fighters_schema', 'lastName')
    ages = db_editor.read_chosen_column('fighters_schema', 'age')
    heights = db_editor.read_chosen_column('fighters_schema', 'height')
    weights = db_editor.read_chosen_column('fighters_schema', 'weight')

    db_editor.close_connection()
